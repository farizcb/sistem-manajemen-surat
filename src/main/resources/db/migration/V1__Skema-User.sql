CREATE TABLE IF NOT EXISTS c_security_permission (
  id varchar(255) NOT NULL,
  permission_label varchar(255) NOT NULL,
  permission_value varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS c_security_role (
  id varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  name varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS c_security_role_permission (
  id_role varchar(255) NOT NULL,
  id_permission varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS c_security_user (
    id varchar(36),
    username varchar(255) not null,
    active boolean not null,
    id_role varchar(255) not null,
    tipe varchar(255) DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS c_security_user_password (
    id_user varchar(36) not null,
    password varchar(255) not null
);

ALTER TABLE c_security_permission
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY UK_k4suda9cvcsoikdgquscypmt6 (permission_value);

ALTER TABLE c_security_role
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY UK_hliaoojt6u3a11d8svttju10l (name);

ALTER TABLE c_security_role_permission
  ADD PRIMARY KEY (id_role,id_permission);

ALTER TABLE c_security_user
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY UK_at8if7a9lnl90wxllb9divpdf (username);

ALTER TABLE c_security_user_password
  ADD PRIMARY KEY (id_user);

ALTER TABLE c_security_role_permission
  ADD CONSTRAINT FKg9os4isbs19ssfahravousxes FOREIGN KEY (id_role) REFERENCES c_security_role (id),
  ADD CONSTRAINT FKnqcv2qdac1phe20qqnyi6n1n FOREIGN KEY (id_permission) REFERENCES c_security_permission (id);

ALTER TABLE c_security_user
  ADD CONSTRAINT FKe5ychpyk27l8vj47v36mrn0s1 FOREIGN KEY (id_role) REFERENCES c_security_role (id);

ALTER TABLE c_security_user_password
  ADD CONSTRAINT FKe5ychpyk27l8vj47v36passwd FOREIGN KEY (id_user) REFERENCES c_security_user (id);