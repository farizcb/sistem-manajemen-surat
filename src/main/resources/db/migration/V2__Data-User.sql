INSERT INTO c_security_role (id, name, description) VALUES
  ('admin', 'Administrator', 'Staff TU'),
  ('superadmin', 'Super Admin', 'Ketua / Sekretaris');

INSERT INTO c_security_permission (id, permission_label, permission_value) VALUES
  ('MENU_LAYANAN_SURAT', 'Menu Layanan Surat', 'ROLE_MENU_LAYANAN_SURAT'),
  ('EDIT_LAYANAN_SURAT', 'Edit Layanan Surat', 'ROLE_EDIT_LAYANAN_SURAT'),
  ('MENU_SURAT_MASUK', 'Menu Surat Masuk', 'ROLE_MENU_SURAT_MASUK'),
  ('EDIT_SURAT_MASUK', 'Edit Surat Masuk', 'ROLE_EDIT_SURAT_MASUK'),
  ('MENU_DISPOSISI', 'Menu Disposisi', 'ROLE_MENU_DISPOSISI'),
  ('EDIT_DISPOSISI', 'Edit Disposisi', 'ROLE_EDIT_DISPOSISI'),
  ('MENU_SURAT_KELUAR', 'Menu Surat Keluar', 'ROLE_MENU_SURAT_KELUAR'),
  ('EDIT_SURAT_KELUAR', 'Edit Surat Keluar', 'ROLE_EDIT_SURAT_KELUAR'),
  ('MENU_INSTANSI', 'Menu Instansi', 'ROLE_MENU_INSTANSI'),
  ('MENU_JENIS_SURAT', 'Menu Jenis Surat', 'ROLE_MENU_JENIS_SURAT'),
  ('MENU_USER', 'Menu User', 'ROLE_MENU_USER');

INSERT INTO c_security_role_permission(id_role, id_permission) VALUES
  ('admin', 'MENU_LAYANAN_SURAT'),
  ('admin', 'MENU_SURAT_MASUK'),
  ('admin', 'EDIT_SURAT_MASUK'),
  ('admin', 'MENU_DISPOSISI'),
  ('admin', 'MENU_SURAT_KELUAR'),
  ('admin', 'EDIT_SURAT_KELUAR'),
  ('admin', 'MENU_INSTANSI'),
  ('admin', 'MENU_JENIS_SURAT'),
  ('admin', 'MENU_USER'),
  ('superadmin', 'MENU_LAYANAN_SURAT'),
  ('superadmin', 'EDIT_LAYANAN_SURAT'),
  ('superadmin', 'MENU_SURAT_MASUK'),
  ('superadmin', 'MENU_DISPOSISI'),
  ('superadmin', 'EDIT_DISPOSISI'),
  ('superadmin', 'MENU_SURAT_KELUAR');

INSERT INTO c_security_user (id, active, username, id_role, tipe) VALUES
  ('admin', true, 'admin', 'admin', NULL),
  ('ketua', true, 'ketua', 'superadmin', 'ketua'),
  ('sekretaris', true, 'sekretaris', 'superadmin', 'sekretaris');

INSERT INTO c_security_user_password (id_user, password) VALUES
  ('admin', '$2a$08$LS3sz9Ft014MNaIGCEyt4u6VflkslOW/xosyRbinIF9.uaVLpEhB6'),
  ('ketua', '$2a$13$5NULQBMoqvzXNRM.EY5ope8FgrIR2tYNfbH8JG2gn2PXLq4vp8Emy'),
  ('sekretaris', '$2a$13$oA1pCks9KVhSZDe3pkFNOeX/1xDBNGgu6qfctjLVRTjcEhP5jyST2');