CREATE TABLE `m_disposisi` (
  `id` varchar(36) NOT NULL,
  `batas_waktu` datetime DEFAULT NULL,
  `informasi` text NOT NULL,
  `isi` text NOT NULL,
  `sifat` varchar(255) NOT NULL,
  `tujuan_disposisi` varchar(255) NOT NULL,
  `id_surat_masuk` varchar(36) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `m_instansi` (
  `id` varchar(36) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `nama_instansi` varchar(255) NOT NULL,
  `no_telepon` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `m_jenis_surat` (
  `id` varchar(36) NOT NULL,
  `jenis_surat` varchar(255) NOT NULL,
  `warna` varchar(255) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `m_layanan_surat` (
  `id` varchar(36) NOT NULL,
  `deskripsi` text NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `perihal` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tanggal_dibuat` datetime DEFAULT NULL,
  `instansi` varchar(255) NOT NULL,
  `id_jenis_surat` varchar(36) NOT NULL,
  `id_user_admin` varchar(36) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `m_surat_keluar` (
  `id` varchar(36) NOT NULL,
  `deskripsi` text NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `kode_surat` varchar(255) NOT NULL,
  `no_surat` varchar(255) NOT NULL,
  `perihal` varchar(255) NOT NULL,
  `tanggal_catat` datetime DEFAULT NULL,
  `tanggal_surat` datetime DEFAULT NULL,
  `id_instansi` varchar(36) NOT NULL,
  `id_jenis_surat` varchar(36) NOT NULL,
  `id_surat_masuk` varchar(36) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `m_surat_masuk` (
  `id` varchar(36) NOT NULL,
  `deskripsi` text NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) NOT NULL,
  `kode_surat` varchar(255) NOT NULL,
  `no_surat` varchar(255) NOT NULL,
  `perihal` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tanggal_catat` datetime DEFAULT NULL,
  `tanggal_surat` datetime DEFAULT NULL,
  `id_instansi` varchar(36) NOT NULL,
  `id_jenis_surat` varchar(36) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `m_user_admin` (
  `id` varchar(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `no_telepon` varchar(255) NOT NULL,
  `npa` varchar(255) NOT NULL,
  `id_user` varchar(36) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `m_disposisi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK26d431a36kmleji458bc4730k` (`id_surat_masuk`);

ALTER TABLE `m_instansi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_rnfftew663bsffl0yoyr4kx31` (`email`),
  ADD UNIQUE KEY `UK_3n6go5ck05k7ueup10rn0atl2` (`kode`),
  ADD UNIQUE KEY `UK_l33l806kg9f8tdnorwnjs7gsf` (`no_telepon`);

ALTER TABLE `m_jenis_surat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_f2nituk2k6t1e94b1s2ofttt6` (`jenis_surat`);

ALTER TABLE `m_layanan_surat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK4ej8y5ooxhub90i1ayn2dk23s` (`id_jenis_surat`),
  ADD KEY `FK6p8aqr6rkxep2g4vvsordi9r4` (`id_user_admin`);

ALTER TABLE `m_surat_keluar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_4ysna2tcg8eyue6ihom8w1vy1` (`kode_surat`),
  ADD UNIQUE KEY `UK_g0j2aqfquhqfn9mh5vfg10qvg` (`no_surat`),
  ADD KEY `FKfc91h3wmk7vfalgh53p731954` (`id_instansi`),
  ADD KEY `FKlvsi5903nxa3gh9t6hb20qusk` (`id_jenis_surat`),
  ADD KEY `FKlt37mmlvey5ql6ul8kxqfr6g9` (`id_surat_masuk`);

ALTER TABLE `m_surat_masuk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_6lfnlmvahs9fxkhk903thkk86` (`kode_surat`),
  ADD UNIQUE KEY `UK_kuruhcdiuljycqjm28shpf942` (`no_surat`),
  ADD KEY `FK3ooujb8jyiuliofomdyl0q26w` (`id_instansi`),
  ADD KEY `FK4qmot2d4vmuoto8kjrpl3urw7` (`id_jenis_surat`);

ALTER TABLE `m_user_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_dwyylv79p5petnfuex6ig61op` (`email`),
  ADD UNIQUE KEY `UK_368cpuo3v0p03iqi8l3frn9xy` (`no_telepon`),
  ADD UNIQUE KEY `UK_i93vdx4e15hh181h10q4fy7a` (`npa`),
  ADD KEY `FKef4hu8c8oa29n3o24u2vhd3eh` (`id_user`);

