package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.dao.InstansiDao;
import com.java.sistem.manajemen.surat.dao.JenisSuratDao;
import com.java.sistem.manajemen.surat.dao.SuratMasukDao;
import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.dto.BasicSearchDto;
import com.java.sistem.manajemen.surat.entity.SuratMasuk;
import com.java.sistem.manajemen.surat.entity.UserAdmin;
import com.java.sistem.manajemen.surat.helper.DateHelper;
import com.java.sistem.manajemen.surat.helper.FileReaderHelper;
import com.java.sistem.manajemen.surat.service.PdfService;
import com.java.sistem.manajemen.surat.service.SuratMasukService;
import com.java.sistem.manajemen.surat.service.UploadFileService;
import java.io.File;
import java.net.MalformedURLException;
import java.security.Principal;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/surat_masuk")
public class SuratMasukController {

    private static final String SHOW_LIST = "surat_masuk/list";
    private static final String SHOW_FORM = "surat_masuk/form";

    @Autowired
    private SuratMasukDao suratMasukDao;

    @Autowired
    private JenisSuratDao jenisSuratDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private UserAdminDao userAdminDao;

    @Autowired
    private SuratMasukService suratMasukService;

    @Autowired
    private UploadFileService uploadFileService;

    @Autowired
    private PdfService pdfService;

    private static final Logger logger = LoggerFactory.getLogger(SuratMasukController.class);

    @GetMapping("/list")
    public String showList(ModelMap modelMap, @PageableDefault Pageable pageable, BasicSearchDto basicSearchDto) {
        if (StringUtils.hasText(basicSearchDto.getQuery()) && StringUtils.hasText(basicSearchDto.getValue())) {
            Date startDate = DateHelper.getDynamicStartDateOfMonth(Integer.valueOf(basicSearchDto.getQuery()));
            Date endDate = DateHelper.getDynamicEndDateOfMonth(startDate);
            modelMap.addAttribute("listData", suratMasukDao.findByTanggalSuratBetweenAndKodeSuratContainingIgnoreCaseOrTanggalSuratBetweenAndInstansiNamaInstansiContainingIgnoreCaseOrTanggalSuratBetweenAndJenisSuratJenisSuratContainingIgnoreCaseOrTanggalSuratBetweenAndNoSuratContainingIgnoreCaseOrTanggalSuratBetweenAndPerihalContainingIgnoreCaseOrOrTanggalSuratBetweenAndDeskripsiContainingIgnoreCase(startDate, endDate, basicSearchDto.getValue(),
                    startDate, endDate, basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), startDate, endDate,
                    basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), pageable));
        } else {
            modelMap.addAttribute("listData", suratMasukDao.findAll(pageable));
        }
        modelMap.addAttribute("paramSearch", basicSearchDto);
        return SHOW_LIST;
    }

    @GetMapping("/form")
    public String showForm(ModelMap modelMap, @RequestParam(required = false) String id) {
        SuratMasuk suratMasuk = new SuratMasuk();
        if (StringUtils.hasText(id)) {
            Optional<SuratMasuk> opSuratMasuk = suratMasukDao.findById(id);
            if (opSuratMasuk.isPresent()) {
                suratMasuk = opSuratMasuk.get();
            }
        }
        suratMasukService.suratMasukModel(modelMap, suratMasuk, jenisSuratDao.findAll(), instansiDao.findAll());
        return SHOW_FORM;
    }

    @PostMapping("/form")
    public String prosesForm(SuratMasuk suratMasuk, ModelMap modelMap, RedirectAttributes redirectAttributes, MultipartFile fileSuratMasuk, Principal principal) {
        logger.info("surat masuk request : [{}]", suratMasuk);

        // validasi surat masuk
        Map<String, Object> mapValidasi = suratMasukService.validasiSimpanSuratMasuk(suratMasuk);
        boolean isValidate = (boolean) mapValidasi.get("isValidate");
        String message = (String) mapValidasi.get("message");
        if (isValidate) {
            modelMap.addAttribute("errorMessage", message);
            suratMasukService.suratMasukModel(modelMap, suratMasuk, jenisSuratDao.findAll(), instansiDao.findAll());
            return SHOW_FORM;
        }

        // simpan surat masuk
        try {
            if (StringUtils.isEmpty(suratMasuk.getId())) {
                // generate kode surat
                suratMasuk.setKodeSurat(suratMasukService.generateKodeSurat(suratMasuk.getTanggalSurat()));
                suratMasuk.setKeterangan("hadir");
            } else {
                UserAdmin userAdmin = userAdminDao.findByEmail(principal.getName());
                if (userAdmin != null) {
                    Optional<SuratMasuk> optionalSuratMasuk = suratMasukDao.findById(suratMasuk.getId());
                    if (optionalSuratMasuk.isPresent()) {
                        if (userAdmin.getUser().getRole().getName().equals("Super Admin")) {
                            String keterangan = suratMasuk.getKeterangan();
                            suratMasuk = optionalSuratMasuk.get();
                            suratMasuk.setKeterangan(keterangan);
                        } else {
                            SuratMasuk suratMasuk1 = optionalSuratMasuk.get();
                            suratMasuk.setKeterangan(suratMasuk1.getKeterangan());
                            suratMasuk.setKodeSurat(suratMasuk1.getKodeSurat());
                        }
                    }
                } else {
                    logger.info("User with username : [{}] not found !", principal.getName());
                }
            }
            if (fileSuratMasuk != null && !fileSuratMasuk.isEmpty()) {
                try {
                    String strFile = uploadFileService.uploadFile(fileSuratMasuk, "suratMasuk");
                    if (StringUtils.hasText(strFile)) {
                        suratMasuk.setFile(strFile);
                    } else {
                        modelMap.addAttribute("errorMessage", "Gagal upload file surat masuk karena : extension yang diperbolehkan hanya {docx, doc, pdf dan odt}");
                        suratMasukService.suratMasukModel(modelMap, suratMasuk, jenisSuratDao.findAll(), instansiDao.findAll());
                        return SHOW_FORM;
                    }
                } catch (Exception e) {
                    modelMap.addAttribute("errorMessage", "Gagal upload file surat masuk karena : [" + e.getMessage() + "]");
                    suratMasukService.suratMasukModel(modelMap, suratMasuk, jenisSuratDao.findAll(), instansiDao.findAll());
                    return SHOW_FORM;
                }
            }
            logger.info("do save suratMasuk : [{}]", suratMasuk);
            suratMasukDao.save(suratMasuk);
            String successMessage;
            if (StringUtils.hasText(suratMasuk.getId())) {
                successMessage = "Data berhasil diubah";
            } else {
                successMessage = "Data berhasil ditambah";
            }
            redirectAttributes.addFlashAttribute("successMessage", successMessage);
            return "redirect:/" + SHOW_LIST;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Data gagal disimpan karena : [" + e.getMessage() + "]");
            suratMasukService.suratMasukModel(modelMap, suratMasuk, jenisSuratDao.findAll(), instansiDao.findAll());
            return SHOW_FORM;
        }
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redirectAttributes) {
        if (StringUtils.hasText(id)) {
            try {
                Optional<SuratMasuk> optionalSuratMasuk = suratMasukDao.findById(id);
                if (optionalSuratMasuk.isPresent()) {
                    suratMasukDao.delete(optionalSuratMasuk.get());
                    redirectAttributes.addFlashAttribute("successMessage", "Data berhasil dihapus");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("errorMessage", "Gagal menghapus data karena data tersebut berelasi dengan tabel lain");
            }
        }
        return "redirect:/" + SHOW_LIST;
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> download(HttpServletRequest request, RedirectAttributes redir, @RequestParam String id) throws ParseException {
        ResponseEntity<Resource> responseEntity;
        try {
            SuratMasuk suratMasuk;
            Optional<SuratMasuk> opSuratMasuk = suratMasukDao.findById(id);
            if (opSuratMasuk.isPresent()) {
                suratMasuk = opSuratMasuk.get();
            } else {
                return null;
            }

            if (StringUtils.hasText(suratMasuk.getFile())) {
                String fileLocation = pdfService.uploadDir + File.separator + UploadFileService.SURAT_MASUK + File.separator + suratMasuk.getFile();
                Resource file = FileReaderHelper.loadAsResource(fileLocation);
                responseEntity = ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + file.getFilename() + "\"").body(file);
            } else {
                return null;
            }
            return responseEntity;
        } catch (MalformedURLException ex) {
            redir.addFlashAttribute("errorMessage", ex.getMessage());
            return null;
        }
    }

}
