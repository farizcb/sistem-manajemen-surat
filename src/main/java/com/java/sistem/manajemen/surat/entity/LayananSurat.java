package com.java.sistem.manajemen.surat.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "m_layanan_surat")
@Data
public class LayananSurat extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_jenis_surat", nullable = false)
    private JenisSurat jenisSurat;

    @Column(nullable = false)
    private String instansi;
    
    @ManyToOne
    @JoinColumn(name = "id_user_admin", nullable = false)
    private UserAdmin userAdmin;
    
    @Column(nullable = false)
    private String perihal;
    
    private String status;
    
    @Lob
    @Type(type = "text")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String deskripsi;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date tanggalDibuat = new Date();
    
    private String file;
    
}
