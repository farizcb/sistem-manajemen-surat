package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.builder.DisposisiBuilder;
import com.java.sistem.manajemen.surat.dao.DisposisiDao;
import com.java.sistem.manajemen.surat.dao.SuratMasukDao;
import com.java.sistem.manajemen.surat.dto.BasicSearchDto;
import com.java.sistem.manajemen.surat.entity.Disposisi;
import com.java.sistem.manajemen.surat.entity.SuratMasuk;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/disposisi")
public class DisposisiController {
    
    private static final String SHOW_LIST = "disposisi/list";
    private static final String SHOW_FORM = "disposisi/form";
    
    @Autowired
    private DisposisiDao disposisiDao;
    
    @Autowired
    private SuratMasukDao suratMasukDao;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DisposisiController.class);
    
    @GetMapping("/list")
    public String showList(ModelMap modelMap, @PageableDefault Pageable pageable, BasicSearchDto basicSearchDto) {
        if (StringUtils.hasText(basicSearchDto.getQuery())) {
            DisposisiBuilder disposisiBuilder = new DisposisiBuilder();
            disposisiBuilder.with(basicSearchDto.getQuery(), "like", basicSearchDto.getValue());
            modelMap.addAttribute("listData", disposisiDao.findAll(disposisiBuilder.build(), pageable));
        } else {
            modelMap.addAttribute("listData", disposisiDao.findAll(pageable));
        }
        modelMap.addAttribute("paramSearch", basicSearchDto);
        return SHOW_LIST;
    }
    
    @GetMapping("/form")
    public String showForm(ModelMap modelMap, @RequestParam(required = false) String id, @RequestParam(required = false) String idSuratMasuk) {
        Disposisi disposisi = new Disposisi();
        if (StringUtils.hasText(idSuratMasuk)) {
            Optional<SuratMasuk> opSuratMasuk = suratMasukDao.findById(idSuratMasuk);
            if (opSuratMasuk.isPresent()) {
                Disposisi d = disposisiDao.findBySuratMasukId(opSuratMasuk.get().getId());
                if (d != null) {
                    disposisi = d;
                } else {
                    disposisi.setSuratMasuk(opSuratMasuk.get());
                }
            }
        }
        if (StringUtils.hasText(id)) {
            Optional<Disposisi> opDisposisi = disposisiDao.findById(id);
            if (opDisposisi.isPresent()) {
                disposisi = opDisposisi.get();
            } else {
                LOGGER.error("Disposisi with id : [{}] not found", id);
            }
        }
        modelMap.addAttribute("disposisi", disposisi);
        return SHOW_FORM;
    }
    
    @PostMapping("/form")
    public String prosesForm(Disposisi disposisi, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        // simpan disposisi
        try {
            LOGGER.info("do save disposisi : [{}]", disposisi);
            disposisiDao.save(disposisi);
            String successMessage;
            if (StringUtils.hasText(disposisi.getId())) {
                successMessage = "Data berhasil diubah";
            } else {
                successMessage = "Data berhasil ditambah";
            }
            redirectAttributes.addFlashAttribute("successMessage", successMessage);
            return "redirect:/" + SHOW_LIST;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Data gagal disimpan karena : [" + e.getMessage() + "]");
            modelMap.addAttribute("disposisi", disposisi);
            LOGGER.error("Failed insert disposisi");
            e.printStackTrace();
            return SHOW_FORM;
        }
    }
    
}
