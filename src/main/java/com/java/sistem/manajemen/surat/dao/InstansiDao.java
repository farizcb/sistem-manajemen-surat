package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.Instansi;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InstansiDao extends PagingAndSortingRepository<Instansi, String>, JpaSpecificationExecutor<Instansi> {

    Instansi findByNoTelepon(String noTelepon);

    Instansi findByEmail(String email);

    Instansi findByKode(String kode);

}
