package com.java.sistem.manajemen.surat.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity 
@Table(name = "c_security_permission")
@Data
public class Permission extends BaseEntity {

    @NotNull
    @Column(name = "permission_label", nullable = false, unique = false)
    private String label;

    @NotNull
    @Column(name = "permission_value", nullable = false, unique = true)
    private String value;
    
}
