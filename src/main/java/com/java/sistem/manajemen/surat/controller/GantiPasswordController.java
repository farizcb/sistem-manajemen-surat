package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.dto.GantiPasswordDto;
import com.java.sistem.manajemen.surat.entity.UserAdmin;
import com.java.sistem.manajemen.surat.entity.UserPassword;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/ganti_password")
public class GantiPasswordController {

    private static final String SHOW_FORM = "ganti_password";

    @Autowired
    private UserAdminDao userAdminDao;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/form")
    public String showForm(@RequestParam String id, ModelMap modelMap) {
        GantiPasswordDto gantiPasswordDto = new GantiPasswordDto();
        Optional<UserAdmin> opUserAdmin = userAdminDao.findById(id);
        if (opUserAdmin.isPresent()) {
            gantiPasswordDto.setId(opUserAdmin.get().getId());
        }
        modelMap.addAttribute("gantiPasswordDto", gantiPasswordDto);
        return SHOW_FORM;
    }
    
    @PostMapping("/form")
    public String prosesForm(GantiPasswordDto gantiPasswordDto, RedirectAttributes redirectAttributes) {
        Optional<UserAdmin> opUserAdmin = userAdminDao.findById(gantiPasswordDto.getId());
        UserPassword userPassword;
        if (opUserAdmin.isPresent()) {
            UserAdmin userAdmin = opUserAdmin.get();
            userPassword = userAdmin.getUser().getUserPassword();
            userPassword.setPassword(passwordEncoder.encode(gantiPasswordDto.getPassword()));
            userPassword.setUser(userAdmin.getUser());
            userAdminDao.save(userAdmin);
        }
        redirectAttributes.addFlashAttribute("successMessage", "Berhasil Ganti Password");
        return "redirect:/ganti_password/form?id=" + gantiPasswordDto.getId();
    }

}
