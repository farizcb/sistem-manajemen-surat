package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.SuratMasuk;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SuratMasukDao extends PagingAndSortingRepository<SuratMasuk, String> {

    SuratMasuk findByNoSurat(String noSurat);
    
    Page<SuratMasuk> findByTanggalSuratBetween(Date startDate, Date endDate, Pageable pageable);
    
    List<SuratMasuk> findByTanggalSuratBetween(Date startDate, Date endDate);
    
    Page<SuratMasuk> findByTanggalSuratBetweenAndKodeSuratContainingIgnoreCaseOrTanggalSuratBetweenAndInstansiNamaInstansiContainingIgnoreCaseOrTanggalSuratBetweenAndJenisSuratJenisSuratContainingIgnoreCaseOrTanggalSuratBetweenAndNoSuratContainingIgnoreCaseOrTanggalSuratBetweenAndPerihalContainingIgnoreCaseOrOrTanggalSuratBetweenAndDeskripsiContainingIgnoreCase
        (Date startDate, Date endDate, String kodeSurat, Date startDate2, Date endDate2, String namaInstansi, Date startDate3, Date endDate3, String jenisSurat, 
                Date startDate4, Date endDate4, String noSurat, Date startDate5, Date endDate5, String perihal, Date startDate6, Date endDate6, String deskripsi, Pageable pageable);
    
    long countByJenisSuratId(String idJenisSurat);

    long countByJenisSuratIdAndKeterangan(String idJenisSurat, String keterangan);

    long countByJenisSuratIdAndTanggalSuratBetween(String idJenisSurat, Date startDate, Date endDate);

}
