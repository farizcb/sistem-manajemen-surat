package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.dao.LayananSuratDao;
import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.dto.BasicSearchDto;
import com.java.sistem.manajemen.surat.entity.LayananSurat;
import com.java.sistem.manajemen.surat.entity.UserAdmin;
import com.java.sistem.manajemen.surat.helper.DateHelper;
import com.java.sistem.manajemen.surat.helper.FileReaderHelper;
import com.java.sistem.manajemen.surat.service.LayananSuratService;
import com.java.sistem.manajemen.surat.service.PdfService;
import com.java.sistem.manajemen.surat.service.UploadFileService;
import java.io.File;
import java.net.MalformedURLException;
import java.security.Principal;
import java.text.ParseException;
import java.util.Date;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/layanan_surat")
public class LayananSuratController {

    private static final String SHOW_LIST = "layanan_surat/list";
    private static final String SHOW_FORM = "layanan_surat/form";

    @Autowired
    private LayananSuratDao layananSuratDao;

    @Autowired
    private UserAdminDao userAdminDao;

    @Autowired
    private LayananSuratService layananSuratService;

    @Autowired
    private UploadFileService uploadFileService;

    @Autowired
    private PdfService pdfService;

    private static final Logger LOGGER = LoggerFactory.getLogger(LayananSuratController.class);

    @GetMapping("/list")
    public String showList(ModelMap modelMap, @PageableDefault Pageable pageable, BasicSearchDto basicSearchDto) {
        if (StringUtils.hasText(basicSearchDto.getQuery()) && StringUtils.hasText(basicSearchDto.getValue())) {
            Date startDate = DateHelper.getDynamicStartDateOfMonth(Integer.valueOf(basicSearchDto.getQuery()));
            Date endDate = DateHelper.getDynamicEndDateOfMonth(startDate);
            modelMap.addAttribute("listData", layananSuratDao.findByTanggalDibuatBetweenAndInstansiContainingIgnoreCaseOrTanggalDibuatBetweenAndJenisSuratJenisSuratContainingIgnoreCaseOrTanggalDibuatBetweenAndPerihalContainingIgnoreCaseOrOrTanggalDibuatBetweenAndDeskripsiContainingIgnoreCase(startDate, endDate, basicSearchDto.getValue(),
                    startDate, endDate, basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), pageable));
        } else {
            modelMap.addAttribute("listData", layananSuratDao.findAll(pageable));
        }
        modelMap.addAttribute("paramSearch", basicSearchDto);
        return SHOW_LIST;
    }

    @GetMapping("/form")
    public String showForm(ModelMap modelMap, @RequestParam(required = false) String id) {
        LayananSurat layananSurat = new LayananSurat();
        if (StringUtils.hasText(id)) {
            Optional<LayananSurat> opLayananSurat = layananSuratDao.findById(id);
            if (opLayananSurat.isPresent()) {
                layananSurat = opLayananSurat.get();
            }
        }
        layananSuratService.layananSuratModel(modelMap, layananSurat);
        return SHOW_FORM;
    }

    @PostMapping("/form")
    public String prosesForm(LayananSurat layananSurat, ModelMap modelMap, RedirectAttributes redirectAttributes, Principal principal, MultipartFile fileLayananSurat) {
        // simpan layanan surat
        try {
            UserAdmin userAdmin = userAdminDao.findByEmail(principal.getName());
            if (userAdmin != null) {
                if (userAdmin.getUser().getRole().getName().equals("Super Admin")) {
                    layananSurat.setStatus("belumSelesai");
                } else {
                    Optional<LayananSurat> opLayananSurat = layananSuratDao.findById(layananSurat.getId());
                    if (opLayananSurat.isPresent()) {
                        String status = layananSurat.getStatus();
                        layananSurat = opLayananSurat.get();
                        layananSurat.setStatus(status);
                    }
                }
                if (fileLayananSurat != null && !fileLayananSurat.isEmpty()) {
                    try {
                        String strFile = uploadFileService.uploadFile(fileLayananSurat, "layananSurat");
                        if (StringUtils.hasText(strFile)) {
                            layananSurat.setFile(strFile);
                        } else {
                            modelMap.addAttribute("errorMessage", "Gagal upload file layanan surat karena : extension yang diperbolehkan hanya {docx, doc, pdf dan odt}");
                            layananSuratService.layananSuratModel(modelMap, layananSurat);
                            return SHOW_FORM;
                        }
                    } catch (Exception e) {
                        modelMap.addAttribute("errorMessage", "Gagal upload file layanan surat karena : [" + e.getMessage() + "]");
                        layananSuratService.layananSuratModel(modelMap, layananSurat);
                        LOGGER.error("Failed upload file layananSurat");
                        e.printStackTrace();
                        return SHOW_FORM;
                    }
                }
                LOGGER.info("do save layananSurat : [{}]", layananSurat);
                layananSuratDao.save(layananSurat);
                String successMessage;
                if (StringUtils.hasText(layananSurat.getId())) {
                    successMessage = "Data berhasil diubah";
                } else {
                    successMessage = "Data berhasil ditambah";
                }
                redirectAttributes.addFlashAttribute("successMessage", successMessage);
            } else {
                LOGGER.info("User with username : [{}] not found !", principal.getName());
            }
            return "redirect:/" + SHOW_LIST;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Data gagal disimpan karena : [" + e.getMessage() + "]");
            layananSuratService.layananSuratModel(modelMap, layananSurat);
            LOGGER.error("Failed insert layananSurat");
            e.printStackTrace();
            return SHOW_FORM;
        }
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redirectAttributes) {
        if (StringUtils.hasText(id)) {
            try {
                Optional<LayananSurat> opLayananSurat = layananSuratDao.findById(id);
                if (opLayananSurat.isPresent()) {
                    layananSuratDao.delete(opLayananSurat.get());
                    redirectAttributes.addFlashAttribute("successMessage", "Data berhasil dihapus");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("errorMessage", "Gagal menghapus data karena data tersebut berelasi dengan tabel lain");
            }
        }
        return "redirect:/" + SHOW_LIST;
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> download(HttpServletRequest request, RedirectAttributes redir, @RequestParam String id) throws ParseException {
        ResponseEntity<Resource> responseEntity;
        try {
            LayananSurat layananSurat;
            Optional<LayananSurat> opLayananSurat = layananSuratDao.findById(id);
            if (opLayananSurat.isPresent()) {
                layananSurat = opLayananSurat.get();
            } else {
                return null;
            }

            if (StringUtils.hasText(layananSurat.getFile())) {
                String fileLocation = pdfService.uploadDir + File.separator + UploadFileService.LAYANAN_SURAT + File.separator + layananSurat.getFile();
                Resource file = FileReaderHelper.loadAsResource(fileLocation);
                responseEntity = ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + file.getFilename() + "\"").body(file);
            } else {
                return null;
            }
            return responseEntity;
        } catch (MalformedURLException ex) {
            redir.addFlashAttribute("errorMessage", ex.getMessage());
            return null;
        }
    }
}
