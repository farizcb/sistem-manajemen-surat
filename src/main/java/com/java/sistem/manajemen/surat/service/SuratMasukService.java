package com.java.sistem.manajemen.surat.service;

import com.java.sistem.manajemen.surat.dao.SuratMasukDao;
import com.java.sistem.manajemen.surat.entity.Instansi;
import com.java.sistem.manajemen.surat.entity.JenisSurat;
import com.java.sistem.manajemen.surat.entity.SuratMasuk;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class SuratMasukService {
    
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("EEEE, dd MMMM yyyy", new Locale("in", "ID"));
    
    @Autowired
    private SuratMasukDao suratMasukDao;
    
    public Map<String, Object> validasiSimpanSuratMasuk(SuratMasuk suratMasuk) {
        Map<String, Object> resultValidate = new HashMap<>();
        boolean isValidate = false;
        String message = "";
        
        // validasi berdasarkan no surat
        SuratMasuk suratMasuk1 = suratMasukDao.findByNoSurat(suratMasuk.getNoSurat());
        if (suratMasuk1 != null && !suratMasuk1.getId().equals(suratMasuk.getId())) {
            isValidate = true;
            message = "No Surat : [" + suratMasuk.getNoSurat() + "] sudah digunakan";
        }
        
        resultValidate.put("isValidate", isValidate);
        resultValidate.put("message", message);

        return resultValidate;
    }
    
    public void suratMasukModel(ModelMap mm, SuratMasuk suratMasuk, Iterable<JenisSurat> listJenisSurat, Iterable<Instansi> listInstansi) {
        mm.addAttribute("suratMasuk", suratMasuk);
        mm.addAttribute("listJenisSurat", listJenisSurat);
        mm.addAttribute("listInstansi", listInstansi);
    }
    
    public String generateKodeSurat(Date tanggalSurat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
        String strTanggalSurat = dateFormat.format(tanggalSurat);
        List<SuratMasuk> listSuratMasuk = (List<SuratMasuk>) suratMasukDao.findAll();
        String defaultFormat = "PGRI/SM/";
        return defaultFormat.concat(strTanggalSurat).concat("-").concat(String.valueOf(listSuratMasuk.size() + 1));
    }
    
    public String convertToExcel(String templateFile, HttpServletResponse response, Date sDate, Date eDate) throws IOException, InvalidFormatException {

        String filename = ("laporan_agenda_surat_masuk-".concat(String.valueOf(new Date().getTime())).concat(".xls")).toUpperCase();
        String resultFile = System.getProperty("java.io.tmpdir") + File.separator + filename;

        // Build Data For Excel
        List<SuratMasuk> listSuratMasuk = suratMasukDao.findByTanggalSuratBetween(sDate, eDate);
        formattingDate(listSuratMasuk);

        Map<String, Object> params = new HashMap<>();
        params.put("listData", listSuratMasuk);
        params.put("sDate", SIMPLE_DATE_FORMAT.format(sDate));
        params.put("eDate", SIMPLE_DATE_FORMAT.format(eDate));

        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFile, params, resultFile);

        response.setContentType("application/vnd.ms-excel");
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", filename);

        response.setHeader(headerKey, headerValue);
        return resultFile;
    }
    
    public void formattingDate(List<SuratMasuk> listData) {
        for (SuratMasuk suratMasuk : listData) {
            suratMasuk.setStrTanggalSurat(SIMPLE_DATE_FORMAT.format(suratMasuk.getTanggalSurat()));
        }
    }

}
