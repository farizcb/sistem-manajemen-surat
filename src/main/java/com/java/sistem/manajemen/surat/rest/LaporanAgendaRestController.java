package com.java.sistem.manajemen.surat.rest;

import com.java.sistem.manajemen.surat.helper.DateHelper;
import com.java.sistem.manajemen.surat.helper.FileReaderHelper;
import com.java.sistem.manajemen.surat.service.SuratKeluarService;
import com.java.sistem.manajemen.surat.service.SuratMasukService;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/laporan_agenda")
public class LaporanAgendaRestController {
    
    @Autowired
    private SuratMasukService suratMasukService;
    
    @Autowired
    private SuratKeluarService suratKeluarService;
    
    private static final Logger logger = LoggerFactory.getLogger(LaporanAgendaRestController.class);
    
    @PostMapping("/surat_masuk/cetak")
    public Map<String, Object> cetakSuratMasuk(HttpServletRequest request, HttpServletResponse response) throws ParseException {
        Date startDate = DateHelper.getStartDateOfMonth();
        Date endDate = DateHelper.getLastDateOfMonth();

        if (StringUtils.hasText(request.getParameter("sDate")) && StringUtils.hasText(request.getParameter("eDate"))) {
            startDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("sDate"));
            endDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("eDate"));
        }

        try {
            String templateFile = request.getServletContext().getRealPath("/WEB-INF/excel/surat_masuk.xls");
            String fileReport = suratMasukService.convertToExcel(templateFile, response, startDate, endDate);
            return FileReaderHelper.responseFileExcel(new File(fileReport), Boolean.TRUE);
        } catch (IOException | InvalidFormatException e) {
            logger.error("Error cetak laporan agenda surat masuk : [{}]", e.getMessage());
            return null;
        }
    }
    
    @PostMapping("/surat_keluar/cetak")
    public Map<String, Object> cetakSuratKeluar(HttpServletRequest request, HttpServletResponse response) throws ParseException {
        Date startDate = DateHelper.getStartDateOfMonth();
        Date endDate = DateHelper.getLastDateOfMonth();

        if (StringUtils.hasText(request.getParameter("sDate")) && StringUtils.hasText(request.getParameter("eDate"))) {
            startDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("sDate"));
            endDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("eDate"));
        }

        try {
            String templateFile = request.getServletContext().getRealPath("/WEB-INF/excel/surat_keluar.xls");
            String fileReport = suratKeluarService.convertToExcel(templateFile, response, startDate, endDate);
            return FileReaderHelper.responseFileExcel(new File(fileReport), Boolean.TRUE);
        } catch (IOException | InvalidFormatException e) {
            logger.error("Error cetak laporan agenda surat keluar : [{}]", e.getMessage());
            return null;
        }
    }
    
}
