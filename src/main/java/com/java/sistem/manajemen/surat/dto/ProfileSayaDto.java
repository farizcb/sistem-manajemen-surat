package com.java.sistem.manajemen.surat.dto;

import lombok.Data;

@Data
public class ProfileSayaDto {
    
    private String id;
    private String npa;
    private String namaLengkap;
    private String email;
    private String noTelepon;
    
}
