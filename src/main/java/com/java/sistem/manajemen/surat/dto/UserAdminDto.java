package com.java.sistem.manajemen.surat.dto;

import lombok.Data;

@Data
public class UserAdminDto {
    
    private String id;
    private String npa;
    private String namaLengkap;
    private String email;
    private String noTelepon;
    private String status;
    private String password;
    private String confirmPassword;
    private String role;

}
