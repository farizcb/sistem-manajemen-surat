package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.dao.*;
import com.java.sistem.manajemen.surat.entity.LayananSurat;
import com.java.sistem.manajemen.surat.entity.SuratKeluar;
import com.java.sistem.manajemen.surat.entity.SuratMasuk;
import com.java.sistem.manajemen.surat.service.IndexService;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    
    @Autowired
    private DisposisiDao disposisiDao;
    
    @Autowired
    private SuratMasukDao suratMasukDao;
    
    @Autowired
    private SuratKeluarDao suratKeluarDao;
    
    @Autowired
    private LayananSuratDao layananSuratDao;

    @Autowired
    private JenisSuratDao jenisSuratDao;
    
    @Autowired
    private IndexService indexService;

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
    
    @GetMapping("/")
    public String showIndex(ModelMap mm) {
        
        long totalAmatSegera = disposisiDao.countBySifat("Amat Segera");
        long totalSegera = disposisiDao.countBySifat("Segera");
        long totalBiasa = disposisiDao.countBySifat("Biasa");
        long totalPenting = disposisiDao.countBySifat("Penting");
        
        mm.addAttribute("totalAmatSegera", totalAmatSegera);
        mm.addAttribute("totalSegera", totalSegera);
        mm.addAttribute("totalBiasa", totalBiasa);
        mm.addAttribute("totalPenting", totalPenting);
        
        mm.addAttribute("listDataChartSuratMasuk", indexService.getDataDounatChartSuratMasuk());
        mm.addAttribute("listDataChartSuratKeluar", indexService.getDataDounatChartSuratKeluar());
        
        List<SuratKeluar> listSuratKeluar = (List<SuratKeluar>) suratKeluarDao.findAll();
        List<LayananSurat> listLayananSurat = (List<LayananSurat>) layananSuratDao.findAll();
        
        mm.addAttribute("totalSuratMasuk", suratMasukDao.count());
        mm.addAttribute("totalJumlahAgendaKehadiran", suratMasukDao.countByJenisSuratId("1"));
        mm.addAttribute("totalSuratKeluar", listSuratKeluar.size());
        mm.addAttribute("totalLayananSurat", listLayananSurat.size());
        mm.addAttribute("listJenisSurat", jenisSuratDao.findAll());
        mm.addAttribute("totalYangDihadiri", suratMasukDao.countByJenisSuratIdAndKeterangan("1", "hadir"));
        mm.addAttribute("totalLayananSuratDiproses", layananSuratDao.countByStatus("selesai"));

        return "index";
    }
    
}
