package com.java.sistem.manajemen.surat.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.java.sistem.manajemen.surat.dao.InstansiDao;
import com.java.sistem.manajemen.surat.dao.JenisSuratDao;
import com.java.sistem.manajemen.surat.dao.SuratKeluarDao;
import com.java.sistem.manajemen.surat.dao.SuratMasukDao;
import com.java.sistem.manajemen.surat.entity.SuratKeluar;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class SuratKeluarService {
    
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("EEEE, dd MMMM yyyy", new Locale("in", "ID"));
    
    @Autowired
    private SuratKeluarDao suratKeluarDao;
    
    @Autowired
    private SuratMasukDao suratMasukDao;
    
    @Autowired
    private JenisSuratDao jenisSuratDao;
    
    @Autowired
    private InstansiDao instansiDao;
    
    @Autowired
    private PdfService pdfService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SuratKeluarService.class);
    
    public Map<String, Object> validasiSimpanSuratKeluar(SuratKeluar suratKeluar) {
        Map<String, Object> resultValidate = new HashMap<>();
        boolean isValidate = false;
        String message = "";
        
        // validasi berdasarkan no surat
        SuratKeluar suratMasuk1 = suratKeluarDao.findByNoSurat(suratKeluar.getNoSurat());
        if (suratMasuk1 != null && !suratMasuk1.getId().equals(suratKeluar.getId())) {
            isValidate = true;
            message = "No Surat : [" + suratKeluar.getNoSurat() + "] sudah digunakan";
        }
        
        resultValidate.put("isValidate", isValidate);
        resultValidate.put("message", message);

        return resultValidate;
    }
    
    public void suratKeluarModel(ModelMap mm, SuratKeluar suratKeluar) {
        mm.addAttribute("suratKeluar", suratKeluar);
        mm.addAttribute("listJenisSurat", jenisSuratDao.findAll());
        mm.addAttribute("listInstansi", instansiDao.findAll());
        mm.addAttribute("listSuratMasuk", suratMasukDao.findAll());
    }
    
    public String generateKodeSurat(Date tanggalSurat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
        String strTanggalSurat = dateFormat.format(tanggalSurat);
        List<SuratKeluar> listSuratKeluar = (List<SuratKeluar>) suratKeluarDao.findAll();
        String defaultFormat = "PGRI/SK/";
        return defaultFormat.concat(strTanggalSurat).concat("-").concat(String.valueOf(listSuratKeluar.size() + 1));
    }
    
    public String convertToExcel(String templateFile, HttpServletResponse response, Date sDate, Date eDate) throws IOException, InvalidFormatException {

        String filename = ("laporan_agenda_surat_keluar-".concat(String.valueOf(new Date().getTime())).concat(".xls")).toUpperCase();
        String resultFile = System.getProperty("java.io.tmpdir") + File.separator + filename;

        // Build Data For Excel
        List<SuratKeluar> listSuratKeluar = suratKeluarDao.findByTanggalSuratBetween(sDate, eDate);
        formattingDate(listSuratKeluar);

        Map<String, Object> params = new HashMap<>();
        params.put("listData", listSuratKeluar);
        params.put("sDate", SIMPLE_DATE_FORMAT.format(sDate));
        params.put("eDate", SIMPLE_DATE_FORMAT.format(eDate));

        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFile, params, resultFile);

        response.setContentType("application/vnd.ms-excel");
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", filename);

        response.setHeader(headerKey, headerValue);
        return resultFile;
    }
    
    public void formattingDate(List<SuratKeluar> listData) {
        for (SuratKeluar suratKeluar : listData) {
            suratKeluar.setStrTanggalSurat(SIMPLE_DATE_FORMAT.format(suratKeluar.getTanggalSurat()));
        }
    }
    
    public String generatePDF(HttpServletRequest request, SuratKeluar suratKeluar) {
        
        String content = "<!DOCTYPE html><html><head> <meta charset=\"utf-8\"/> <title>Laporan DATA PRIBADI ANGGOTA</title> <style>.report-box{max-width: 905px; margin: auto; padding: 30px; border: 1px solid #eee; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555;}table{border-collapse: collapse; width: 100%;}.content-header{padding: 20px 0px 5px 20px; font-size: 10pt;}.distance-1{width: 18%}.distance-2{width: 2%}.distance-3{width: 79%}</style></head><body> <div class=\"report-box\"> <h3 style=\"padding: 5px 0px 0px 180px;\">SURAT KELUAR</h3> <hr style=\"height:1px;border:none;color:#504d4d;background-color:#504d4d;\"/> <hr style=\"height:1px;border:none;color:#504d4d;background-color:#504d4d; margin-top: -6px;\"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <table style=\"padding: 5px 0px 40px 0px;\"> <tr> <td class=\\\\ \\ \"distance-1\">Kode Surat</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${kodeSurat}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">Nama Instansi</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${namaInstansi}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">Jenis Surat</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${jenisSurat}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">No Surat</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${noSurat}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">Perihal</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${perihal}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">Isi Surat</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${deskripsi}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">Tanggal Surat</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${tanggalSurat}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">Tanggal Catat</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${tanggalCatat}</td></tr><tr> <td class=\\\\ \\ \"distance-1\">Keterangan</td><td class=\\\\ \\ \"distance-2\">:</td><td class=\\\\ \\ \"distance-3\">${keterangan}</td></tr></table> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </div></body></html>";
        
        Map<String, Object> params = new HashMap<>();
        
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy", new Locale("in", "ID"));
        
        params.put("kodeSurat", suratKeluar.getKodeSurat());
        params.put("namaInstansi", suratKeluar.getInstansi().getNamaInstansi());
        params.put("jenisSurat", suratKeluar.getJenisSurat().getJenisSurat());
        params.put("noSurat", suratKeluar.getNoSurat());
        params.put("perihal", suratKeluar.getPerihal());
        params.put("deskripsi", suratKeluar.getDeskripsi());
        params.put("tanggalSurat", sdf.format(suratKeluar.getTanggalSurat()));
        params.put("tanggalCatat", sdf.format(suratKeluar.getTanggalCatat()));
        params.put("keterangan", suratKeluar.getSuratMasuk().getKeterangan());
        
        String fileLocation = pdfService.uploadDir + File.separator + PdfService.SURAT_KELUAR + File.separator + "-SURAT-KELUAR.pdf";
        
        File outputFile = new File(fileLocation);
        if (!outputFile.exists()) {
            outputFile.getParentFile().mkdirs();
        }
        
        try (OutputStream streamfile = new FileOutputStream(outputFile);) {
            Document document = new com.itextpdf.text.Document();
            PdfWriter writer = com.itextpdf.text.pdf.PdfWriter.getInstance(document, streamfile);
            document.open();
            InputStream is = new ByteArrayInputStream(pdfService.contentMaker(content, params).getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
            document.close();
            streamfile.close();
        } catch (Exception e) {
            LOGGER.error("Error : [{}]", e.getMessage());
        }
        
        return fileLocation;
    }
    
}
