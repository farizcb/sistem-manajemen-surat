package com.java.sistem.manajemen.surat.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadFileService {
    
    private final List<String> FILE_EXTENSION = Arrays.asList("docx", "doc", "pdf", "odt");
    public static final String ORIGINALPREFIX = "ori-";
    public static final String SURAT_MASUK = "surat_masuk";
    public static final String SURAT_KELUAR = "surat_keluar";
    public static final String LAYANAN_SURAT = "layanan_surat";
    
    @Value("${app.folder.upload}")
    public String uploadDir;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadFileService.class);
    
    public File moveFile(MultipartFile multipartFile, String tpe) {
        try {
            String folder = uploadDir + File.separator + tpe + File.separator;
            File folderImages = new File(folder);
            if (!folderImages.exists()) Files.createDirectories(folderImages.toPath());

            String random = UUID.randomUUID().toString().substring(0, 7);

            int len = multipartFile.getOriginalFilename().length();
            String fileName = multipartFile.getOriginalFilename();
            if (len > 20)
                fileName = fileName.substring(len - 10);

            int occurance = StringUtils.countOccurrencesOf(fileName, ".");

            if (occurance > 1)
                for (int i = 0; i < occurance - 1; i++)
                    fileName = fileName.replaceFirst("\\.", "-");

            fileName = fileName.replace(" ", "-");
            fileName = fileName.replace("_", "-");
            fileName = fileName.replaceAll("[^\\w\\-\\.]", "");
            String name = random + "-" + fileName;

            File originalFile = new File(folder + ORIGINALPREFIX + name);
            Files.copy(multipartFile.getInputStream(), originalFile.toPath());

            return originalFile;
        } catch (IOException ex) {
            LOGGER.info("=== ERROR : [{}] ===", ex.getMessage());
            return null;
        }
    }
    
    public String uploadFile(MultipartFile image, String type) {
        if (!image.getOriginalFilename().isEmpty()) {
            String extention = tokenizer(image.getOriginalFilename(), ".");
            if (FILE_EXTENSION.contains(extention.toLowerCase())) {
                File file;
                if ("suratMasuk".equalsIgnoreCase(type)) {
                    file = moveFile(image, UploadFileService.SURAT_MASUK);
                } else if ("suratKeluar".equalsIgnoreCase(type)) {
                    file = moveFile(image, UploadFileService.SURAT_KELUAR);
                } else {
                    file = moveFile(image, UploadFileService.LAYANAN_SURAT);
                }
                return file.getName();
            }
        }
        return "";
    }
    
    public String tokenizer(String originalFilename, String token) {
        StringTokenizer tokenizer = new StringTokenizer(originalFilename, token);
        String result = "";
        while (tokenizer.hasMoreTokens()) {
            result = tokenizer.nextToken();
        }
        return result;
    }
    
}
