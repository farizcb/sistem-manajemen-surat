package com.java.sistem.manajemen.surat.service;

import com.java.sistem.manajemen.surat.dao.InstansiDao;
import com.java.sistem.manajemen.surat.dao.JenisSuratDao;
import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.entity.LayananSurat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class LayananSuratService {

    @Autowired
    private JenisSuratDao jenisSuratDao;

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private UserAdminDao userAdminDao;

    public void layananSuratModel(ModelMap mm, LayananSurat layananSurat) {
        mm.addAttribute("layananSurat", layananSurat);
        mm.addAttribute("listJenisSurat", jenisSuratDao.findAll());
        mm.addAttribute("listInstansi", instansiDao.findAll());
        mm.addAttribute("listUser", userAdminDao.findByUserRoleName("Super Admin"));
    }

}
