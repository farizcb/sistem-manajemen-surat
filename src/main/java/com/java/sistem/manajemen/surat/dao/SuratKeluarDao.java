package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.SuratKeluar;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SuratKeluarDao extends PagingAndSortingRepository<SuratKeluar, String> {
    
    SuratKeluar findByNoSurat(String noSurat);
    
    Page<SuratKeluar> findByTanggalSuratBetween(Date startDate, Date endDate, Pageable pageable);
    
    List<SuratKeluar> findByTanggalSuratBetween(Date startDate, Date endDate);
    
    Page<SuratKeluar> findByTanggalSuratBetweenAndKodeSuratContainingIgnoreCaseOrTanggalSuratBetweenAndInstansiNamaInstansiContainingIgnoreCaseOrTanggalSuratBetweenAndJenisSuratJenisSuratContainingIgnoreCaseOrTanggalSuratBetweenAndNoSuratContainingIgnoreCaseOrTanggalSuratBetweenAndPerihalContainingIgnoreCaseOrOrTanggalSuratBetweenAndDeskripsiContainingIgnoreCase
        (Date startDate, Date endDate, String kodeSurat, Date startDate2, Date endDate2, String namaInstansi, Date startDate3, Date endDate3, String jenisSurat, 
                Date startDate4, Date endDate4, String noSurat, Date startDate5, Date endDate5, String perihal, Date startDate6, Date endDate6, String deskripsi, Pageable pageable);

    long countByJenisSuratIdAndTanggalSuratBetween(String idJenisSurat, Date startDate, Date endDate);
}
