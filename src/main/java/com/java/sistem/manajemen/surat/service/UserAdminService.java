package com.java.sistem.manajemen.surat.service;

import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.dto.UserAdminDto;
import com.java.sistem.manajemen.surat.entity.Role;
import com.java.sistem.manajemen.surat.entity.UserAdmin;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

@Service
public class UserAdminService {
    
    @Autowired
    private UserAdminDao userAdminDao;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UserAdminService.class);
    
    public Map<String, Object> validasiSimpanUser(UserAdminDto userAdminDto) {
        Map<String, Object> resultValidate = new HashMap<>();
        boolean isValidate = false;
        String message = "";
        
        // validasi berdasarkan npa
        UserAdmin userAdmin1 = userAdminDao.findByNpa(userAdminDto.getNpa());
        if (userAdmin1 != null && !userAdmin1.getId().equals(userAdminDto.getId())) {
            isValidate = true;
            message = "NPA : [" + userAdminDto.getNpa() + "] sudah digunakan";
        }
        
        // validasi berdasarkan email
        UserAdmin userAdmin2 = userAdminDao.findByEmail(userAdminDto.getEmail());
        if (userAdmin2 != null && !userAdmin2.getId().equals(userAdminDto.getId())) {
            isValidate = true;
            message = "Email : [" + userAdminDto.getEmail() + "] sudah digunakan";
        }
        
        // validasi berdasarkan nomor telepon
        UserAdmin userAdmin3 = userAdminDao.findByNoTelepon(userAdminDto.getNoTelepon());
        if (userAdmin3 != null && !userAdmin3.getId().equals(userAdminDto.getId())) {
            isValidate = true;
            message = "Nomor Telepon : [" + userAdminDto.getNoTelepon() + "] sudah digunakan";
        }
        
        resultValidate.put("isValidate", isValidate);
        resultValidate.put("message", message);

        return resultValidate;
    }
    
    public void userAdminModel(ModelMap mm, UserAdminDto adminDto, Iterable<Role> listRole) {
        mm.addAttribute("userAdminDto", adminDto);
        mm.addAttribute("listRole", listRole);
    }
    
    public List<UserAdmin> getResultDataUserAdmin(List<UserAdmin> userAdmins, UserAdmin userAdmin) {
        List<UserAdmin> listResult = new ArrayList<>();
        for (UserAdmin userAdmin1 : userAdmins) {
            if (!userAdmin1.getId().equals(userAdmin.getId())) {
                listResult.add(userAdmin1);
            }
        }
        return listResult;
    }
    
    public Page<UserAdmin> convertListUserAdminToPage(List<UserAdmin> listData, Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int pageNumber = pageable.getPageNumber();
        int total = listData.size();
        int startIndex = (pageNumber > 0) ? (pageNumber * pageSize) : 0;
        int lastIndex = (startIndex + pageSize > listData.size()) ? listData.size() : (startIndex + pageSize);
        return new PageImpl(listData.subList(startIndex, lastIndex), pageable, total);
    }
    
}
