package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.criteria.SearchCriteria;
import com.java.sistem.manajemen.surat.dao.JenisSuratDao;
import com.java.sistem.manajemen.surat.dto.BasicSearchDto;
import com.java.sistem.manajemen.surat.entity.JenisSurat;
import com.java.sistem.manajemen.surat.helper.AppHelper;
import com.java.sistem.manajemen.surat.spesification.JenisSuratSpesification;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/jenis_surat")
public class JenisSuratController {

    private static final String SHOW_LIST = "jenis_surat/list";
    private static final String SHOW_FORM = "jenis_surat/form";

    @Autowired
    private JenisSuratDao jenisSuratDao;

    @GetMapping("/list")
    public String showList(ModelMap modelMap, @PageableDefault Pageable pageable, BasicSearchDto basicSearchDto) {
        if (StringUtils.hasText(basicSearchDto.getQuery())) {
            JenisSuratSpesification instansiSpesification = new JenisSuratSpesification(new SearchCriteria(basicSearchDto.getQuery(), ":", basicSearchDto.getValue()));
            modelMap.addAttribute("listData", jenisSuratDao.findAll(instansiSpesification, pageable));
        } else {
            modelMap.addAttribute("listData", jenisSuratDao.findAll(pageable));
        }
        modelMap.addAttribute("paramSearch", basicSearchDto);
        return SHOW_LIST;
    }

    @GetMapping("/form")
    public String showForm(ModelMap modelMap, @RequestParam(required = false) String id) {
        JenisSurat jenisSurat = new JenisSurat();
        if (StringUtils.hasText(id)) {
            Optional<JenisSurat> opInstansi = jenisSuratDao.findById(id);
            if (opInstansi.isPresent()) {
                jenisSurat = opInstansi.get();
            }
        }
        modelMap.addAttribute("jenisSurat", jenisSurat);
        return SHOW_FORM;
    }

    @PostMapping("/form")
    public String prosesForm(JenisSurat jenisSurat, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        // validasi berdasarkan jenis surat
        JenisSurat jenisSurat1 = jenisSuratDao.findByJenisSurat(jenisSurat.getJenisSurat());
        if (jenisSurat1 != null && !jenisSurat1.getId().equals(jenisSurat.getId())) {
            modelMap.addAttribute("errorMessage", "Jenis Surat : [" + jenisSurat.getJenisSurat() + "] sudah digunakan");
            modelMap.addAttribute("jenisSurat", jenisSurat);
            return SHOW_FORM;
        }

        // simpan jenis surat
        try {
            jenisSuratDao.save(jenisSurat);
            redirectAttributes.addFlashAttribute("successMessage", AppHelper.successMessage(jenisSurat.getId()));
            return "redirect:/" + SHOW_LIST;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Data gagal disimpan karena : [" + e.getMessage() + "]");
            modelMap.addAttribute("jenisSurat", jenisSurat);
            return SHOW_FORM;
        }
    }
    
    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redirectAttributes) {
        if (StringUtils.hasText(id)) {
            try {
                Optional<JenisSurat> optionalInstansi = jenisSuratDao.findById(id);
                if (optionalInstansi.isPresent()) {
                    jenisSuratDao.delete(optionalInstansi.get());
                    redirectAttributes.addFlashAttribute("successMessage", "Data berhasil dihapus");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("errorMessage", "Gagal menghapus data karena data tersebut berelasi dengan tabel lain");
            }
        }
        return "redirect:/" + SHOW_LIST;
    }

}
