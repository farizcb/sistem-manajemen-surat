package com.java.sistem.manajemen.surat.dto;

import lombok.Data;

@Data
public class BasicSearchDto {

    private String query;
    private String value;

}
