package com.java.sistem.manajemen.surat.dto;

import lombok.Data;

@Data
public class GantiPasswordDto {
    
    private String id;
    private String password;
    private String confirmPassword;
    
}
