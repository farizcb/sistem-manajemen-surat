package com.java.sistem.manajemen.surat.service;

import com.java.sistem.manajemen.surat.dao.InstansiDao;
import com.java.sistem.manajemen.surat.entity.Instansi;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InstansiService {
    
    @Autowired
    private InstansiDao instansiDao;
    
    public Map<String, Object> validasiSimpanInstansi(Instansi instansi) {
        Map<String, Object> resultValidate = new HashMap<>();
        boolean isValidate = false;
        String message = "";
        
        // validasi berdasarkan nomor telepon
        Instansi instansi1 = instansiDao.findByNoTelepon(instansi.getNoTelepon());
        if (instansi1 != null && !instansi1.getId().equals(instansi.getId())) {
            isValidate = true;
            message = "Nomor Telepon : [" + instansi.getNoTelepon() + "] sudah digunakan";
        }
        
        // validasi berdasarkan email
        Instansi instansi2 = instansiDao.findByEmail(instansi.getEmail());
        if (instansi2 != null && !instansi2.getId().equals(instansi.getId())) {
            isValidate = true;
            message = "Email : [" + instansi.getEmail() + "] sudah digunakan";
        }

        // validasi berdasarkan kode
        Instansi instansi3 = instansiDao.findByKode(instansi.getKode());
        if (instansi3 != null && !instansi3.getId().equals(instansi.getId())) {
            isValidate = true;
            message = "Kode Instansi : [" + instansi3.getKode() + "] sudah digunakan";
        }
        
        resultValidate.put("isValidate", isValidate);
        resultValidate.put("message", message);

        return resultValidate;
    }
    
}
