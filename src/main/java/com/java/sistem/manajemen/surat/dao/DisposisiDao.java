package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.Disposisi;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DisposisiDao extends PagingAndSortingRepository<Disposisi, String>, JpaSpecificationExecutor<Disposisi> {

    Disposisi findBySuratMasukId(String idSuratMasuk);
    
    long countBySifat(String sifat);
    
}
