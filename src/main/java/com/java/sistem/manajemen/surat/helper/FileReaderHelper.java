package com.java.sistem.manajemen.surat.helper;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

public class FileReaderHelper {

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static Map<String, Object> responseFileExcel(File file, Boolean delete) throws IOException {
        FileInputStream fin = new FileInputStream(file);
        Map<String, Object> base64File = new HashMap<>();
        base64File.put("stream", Base64.encodeBase64String(IOUtils.toByteArray(fin)));

        if (delete) {
            FileUtils.deleteQuietly(file);
        }
        return base64File;
    }
    
    public static Path load(String filename) {
        return new File(filename).toPath();
    }

    public static Resource loadAsResource(String filename) throws MalformedURLException {
        Path filePath = load(filename);
        Resource resource = new UrlResource(filePath.toUri());
        if (resource.exists() || resource.isReadable()) {
            return resource;
        } else {
            throw new MalformedURLException("Could not read file: " + filename);
        }
    }

}
