package com.java.sistem.manajemen.surat.helper;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {
    
    private static final LocalDate TODAY = LocalDate.now();
    
    public static Date getDateStartTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 00);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);

        return calendar.getTime();
    }
    
    public static Date getDateEndTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }
    
    public static Date getDynamicStartDateOfMonth(int indexMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, TODAY.getYear());
        calendar.set(Calendar.MONTH, indexMonth - 1);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 00);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
        
        return calendar.getTime();
    }

    public static Date getDynamicEndDateOfMonth(Date startDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }
    
    public static Date getStartDateOfMonth() {
        LocalDate startDate = TODAY.withDayOfMonth(1);
        Date date1 = java.sql.Date.valueOf(startDate);
        return date1;
    }

    public static Date getLastDateOfMonth() {
        LocalDate endDate = TODAY.withDayOfMonth(TODAY.lengthOfMonth());
        Date date2 = java.sql.Date.valueOf(endDate);
        return date2;
    }
    
}
