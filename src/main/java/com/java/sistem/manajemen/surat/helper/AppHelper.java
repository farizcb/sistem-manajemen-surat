package com.java.sistem.manajemen.surat.helper;

import org.springframework.util.StringUtils;

public class AppHelper {

    public static String successMessage(String id) {
        String successMessage;
        if (StringUtils.hasText(id)) {
            successMessage = "Data berhasil diubah";
        } else {
            successMessage = "Data berhasil ditambah";
        }
        return successMessage;
    }

}
