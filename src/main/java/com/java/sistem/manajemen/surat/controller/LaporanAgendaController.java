package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.dao.SuratKeluarDao;
import com.java.sistem.manajemen.surat.dao.SuratMasukDao;
import com.java.sistem.manajemen.surat.dto.SearchLaporanAgendaDto;
import com.java.sistem.manajemen.surat.helper.DateHelper;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/laporan_agenda")
public class LaporanAgendaController {

    private static final String SHOW_LAPORAN_SURAT_MASUK = "laporan_agenda/surat_masuk";
    private static final String SHOW_LAPORAN_SURAT_KELUAR = "laporan_agenda/surat_keluar";

    @Autowired
    private SuratMasukDao suratMasukDao;

    @Autowired
    private SuratKeluarDao suratKeluarDao;

    @GetMapping("/surat_masuk")
    public String showListLaporanSuratMasuk(ModelMap modelMap, @PageableDefault Pageable pageable, SearchLaporanAgendaDto searchLaporanAgendaDto) {
        if (searchLaporanAgendaDto.getStartDate() != null && searchLaporanAgendaDto.getEndDate() != null) {
            Date startDate = DateHelper.getDateStartTime(searchLaporanAgendaDto.getStartDate());
            Date endDate = DateHelper.getDateEndTime(searchLaporanAgendaDto.getEndDate());
            modelMap.addAttribute("listData", suratMasukDao.findByTanggalSuratBetween(startDate, endDate, pageable));
        } else {
            modelMap.addAttribute("listData", suratMasukDao.findAll(pageable));
        }
        modelMap.addAttribute("searchLaporanAgendaDto", searchLaporanAgendaDto);
        return SHOW_LAPORAN_SURAT_MASUK;
    }

    @GetMapping("/surat_keluar")
    public String showListLaporanSuratKeluar(ModelMap modelMap, @PageableDefault Pageable pageable, SearchLaporanAgendaDto searchLaporanAgendaDto) {
        if (searchLaporanAgendaDto.getStartDate() != null && searchLaporanAgendaDto.getEndDate() != null) {
            Date startDate = DateHelper.getDateStartTime(searchLaporanAgendaDto.getStartDate());
            Date endDate = DateHelper.getDateEndTime(searchLaporanAgendaDto.getEndDate());
            modelMap.addAttribute("listData", suratKeluarDao.findByTanggalSuratBetween(startDate, endDate, pageable));
        } else {
            modelMap.addAttribute("listData", suratKeluarDao.findAll(pageable));
        }
        modelMap.addAttribute("searchLaporanAgendaDto", searchLaporanAgendaDto);
        return SHOW_LAPORAN_SURAT_KELUAR;
    }

}
