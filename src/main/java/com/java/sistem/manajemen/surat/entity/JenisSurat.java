package com.java.sistem.manajemen.surat.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "m_jenis_surat")
@Data
public class JenisSurat extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String jenisSurat;
    
    @Lob
    @Type(type = "text")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String keterangan;

    @Column(nullable = false)
    private String warna;
    
}
