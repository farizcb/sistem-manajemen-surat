package com.java.sistem.manajemen.surat.config;

import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.entity.UserAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;

@ControllerAdvice(basePackages = {"com.java.sistem.manajemen.surat.controller"})
public class GlobalControllerAdvice {
    
    @Autowired
    private UserAdminDao userAdminDao;

    @ModelAttribute
    public void globalAttributes(Model model, Principal principal) {
        if (principal != null) {
            String aktifUser = principal.getName();
            UserAdmin userAdmin = userAdminDao.findByEmail(aktifUser);
            if (userAdmin != null) {
                model.addAttribute("userLoggedIn", userAdmin);
            }
        }

    }

}
