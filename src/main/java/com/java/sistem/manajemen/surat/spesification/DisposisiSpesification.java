package com.java.sistem.manajemen.surat.spesification;

import com.java.sistem.manajemen.surat.criteria.SearchCriteria;
import com.java.sistem.manajemen.surat.entity.Disposisi;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class DisposisiSpesification implements Specification<Disposisi> {

    private final SearchCriteria criteria;

    public DisposisiSpesification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<Disposisi> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if ("kodeSurat".equalsIgnoreCase(criteria.getKey())) {
            return criteriaBuilder.like(criteriaBuilder.lower(
                    root.get("suratMasuk").get("kodeSurat")),
                    "%" + criteria.getValue().toString().toLowerCase() + "%");
        }

        if (criteria.getOperation().equalsIgnoreCase("like")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return criteriaBuilder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        }

        return null;
    }
}
