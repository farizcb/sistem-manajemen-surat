package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.LayananSurat;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LayananSuratDao extends PagingAndSortingRepository<LayananSurat, String> {

    Page<LayananSurat> findByTanggalDibuatBetweenAndInstansiContainingIgnoreCaseOrTanggalDibuatBetweenAndJenisSuratJenisSuratContainingIgnoreCaseOrTanggalDibuatBetweenAndPerihalContainingIgnoreCaseOrOrTanggalDibuatBetweenAndDeskripsiContainingIgnoreCase
        (Date startDate1, Date endDate1, String namaInstansi, Date startDate2, Date endDate2, String jenisSurat, 
                Date startDate3, Date endDate3, String perihal, Date startDate4, Date endDate4, String deskripsi, Pageable pageable);
        
    long countByStatus(String status);
    
}
