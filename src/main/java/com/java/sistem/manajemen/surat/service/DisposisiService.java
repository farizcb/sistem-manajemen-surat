package com.java.sistem.manajemen.surat.service;

import com.java.sistem.manajemen.surat.entity.Disposisi;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;

@Service
public class DisposisiService {
    
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("EEEE, dd MMMM yyyy", new Locale("in", "ID"));
    
    public String convertToExcel(String templateFile, HttpServletResponse response, List<Disposisi> listData) throws IOException, InvalidFormatException {

        String filename = ("disposisi-".concat(String.valueOf(new Date().getTime())).concat(".xls")).toUpperCase();
        String resultFile = System.getProperty("java.io.tmpdir") + File.separator + filename;

        formattingDate(listData);

        Map<String, Object> params = new HashMap<>();
        params.put("listData", listData);

        XLSTransformer transformer = new XLSTransformer();
        transformer.transformXLS(templateFile, params, resultFile);

        response.setContentType("application/vnd.ms-excel");
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", filename);

        response.setHeader(headerKey, headerValue);
        return resultFile;
    }
    
    public void formattingDate(List<Disposisi> listData) {
        for (Disposisi disposisi : listData) {
            disposisi.setStrBatasWaktu(SIMPLE_DATE_FORMAT.format(disposisi.getBatasWaktu()));
        }
    }
    
}
