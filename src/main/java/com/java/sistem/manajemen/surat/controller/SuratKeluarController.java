package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.dao.SuratKeluarDao;
import com.java.sistem.manajemen.surat.dto.BasicSearchDto;
import com.java.sistem.manajemen.surat.entity.SuratKeluar;
import com.java.sistem.manajemen.surat.helper.DateHelper;
import com.java.sistem.manajemen.surat.helper.FileReaderHelper;
import com.java.sistem.manajemen.surat.service.PdfService;
import com.java.sistem.manajemen.surat.service.SuratKeluarService;
import com.java.sistem.manajemen.surat.service.UploadFileService;
import java.io.File;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/surat_keluar")
public class SuratKeluarController {

    private static final String SHOW_LIST = "surat_keluar/list";
    private static final String SHOW_FORM = "surat_keluar/form";

    @Autowired
    private SuratKeluarDao suratKeluarDao;

    @Autowired
    private SuratKeluarService suratKeluarService;

    @Autowired
    private UploadFileService uploadFileService;
    
    @Autowired
    private PdfService pdfService;

    private static final Logger LOGGER = LoggerFactory.getLogger(SuratKeluarController.class);

    @GetMapping("/list")
    public String showList(ModelMap modelMap, @PageableDefault Pageable pageable, BasicSearchDto basicSearchDto) {
        if (StringUtils.hasText(basicSearchDto.getQuery()) && StringUtils.hasText(basicSearchDto.getValue())) {
            Date startDate = DateHelper.getDynamicStartDateOfMonth(Integer.valueOf(basicSearchDto.getQuery()));
            Date endDate = DateHelper.getDynamicEndDateOfMonth(startDate);
            modelMap.addAttribute("listData", suratKeluarDao.findByTanggalSuratBetweenAndKodeSuratContainingIgnoreCaseOrTanggalSuratBetweenAndInstansiNamaInstansiContainingIgnoreCaseOrTanggalSuratBetweenAndJenisSuratJenisSuratContainingIgnoreCaseOrTanggalSuratBetweenAndNoSuratContainingIgnoreCaseOrTanggalSuratBetweenAndPerihalContainingIgnoreCaseOrOrTanggalSuratBetweenAndDeskripsiContainingIgnoreCase(startDate, endDate, basicSearchDto.getValue(),
                    startDate, endDate, basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), startDate, endDate,
                    basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), startDate, endDate, basicSearchDto.getValue(), pageable));
        } else {
            modelMap.addAttribute("listData", suratKeluarDao.findAll(pageable));
        }
        modelMap.addAttribute("paramSearch", basicSearchDto);
        return SHOW_LIST;
    }

    @GetMapping("/form")
    public String showForm(ModelMap modelMap, @RequestParam(required = false) String id) {
        SuratKeluar suratKeluar = new SuratKeluar();
        if (StringUtils.hasText(id)) {
            Optional<SuratKeluar> opSuratKeluar = suratKeluarDao.findById(id);
            if (opSuratKeluar.isPresent()) {
                suratKeluar = opSuratKeluar.get();
            }
        }
        suratKeluarService.suratKeluarModel(modelMap, suratKeluar);
        return SHOW_FORM;
    }

    @PostMapping("/form")
    public String prosesForm(SuratKeluar suratKeluar, ModelMap modelMap, RedirectAttributes redirectAttributes, MultipartFile fileSuratKeluar) {
        // validasi surat keluar
        Map<String, Object> mapValidasi = suratKeluarService.validasiSimpanSuratKeluar(suratKeluar);
        boolean isValidate = (boolean) mapValidasi.get("isValidate");
        String message = (String) mapValidasi.get("message");
        if (isValidate) {
            modelMap.addAttribute("errorMessage", message);
            suratKeluarService.suratKeluarModel(modelMap, suratKeluar);
            return SHOW_FORM;
        }

        // simpan surat keluar
        try {
            if (StringUtils.isEmpty(suratKeluar.getId())) {
                // generate kode surat
                suratKeluar.setKodeSurat(suratKeluarService.generateKodeSurat(suratKeluar.getTanggalSurat()));
            } else {
                Optional<SuratKeluar> optionalSuratMasuk = suratKeluarDao.findById(suratKeluar.getId());
                if (optionalSuratMasuk.isPresent()) {
                    suratKeluar.setKodeSurat(optionalSuratMasuk.get().getKodeSurat());
                    suratKeluar.setFile(optionalSuratMasuk.get().getFile());
                }
            }
            if (fileSuratKeluar != null && !fileSuratKeluar.isEmpty()) {
                try {
                    String strFile = uploadFileService.uploadFile(fileSuratKeluar, "suratKeluar");
                    if (StringUtils.hasText(strFile)) {
                        suratKeluar.setFile(strFile);
                    } else {
                        modelMap.addAttribute("errorMessage", "Gagal upload file surat keluar karena : extension yang diperbolehkan hanya {docx, doc, pdf dan odt}");
                        suratKeluarService.suratKeluarModel(modelMap, suratKeluar);
                        return SHOW_FORM;
                    }
                } catch (Exception e) {
                    modelMap.addAttribute("errorMessage", "Gagal upload file surat keluar karena : [" + e.getMessage() + "]");
                    suratKeluarService.suratKeluarModel(modelMap, suratKeluar);
                    return SHOW_FORM;
                }
            }
            LOGGER.info("do save suratKeluar : [{}]", suratKeluar);
            suratKeluarDao.save(suratKeluar);
            String successMessage;
            if (StringUtils.hasText(suratKeluar.getId())) {
                successMessage = "Data berhasil diubah";
            } else {
                successMessage = "Data berhasil ditambah";
            }
            redirectAttributes.addFlashAttribute("successMessage", successMessage);
            return "redirect:/" + SHOW_LIST;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Data gagal disimpan karena : [" + e.getMessage() + "]");
            suratKeluarService.suratKeluarModel(modelMap, suratKeluar);
            return SHOW_FORM;
        }
    }

    @GetMapping("/cetak")
    public ResponseEntity<Resource> cetak(HttpServletRequest request, RedirectAttributes redir, @RequestParam String id) throws ParseException {
        ResponseEntity<Resource> responseEntity;
        try {
            SuratKeluar suratKeluar;
            Optional<SuratKeluar> opSuratKeluar = suratKeluarDao.findById(id);
            if (opSuratKeluar.isPresent()) {
                suratKeluar = opSuratKeluar.get();
            } else {
                return null;
            }

            String templateFile = suratKeluarService.generatePDF(request, suratKeluar);
            Resource file = FileReaderHelper.loadAsResource(templateFile);
            responseEntity = ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + file.getFilename() + "\"").body(file);

        } catch (MalformedURLException ex) {
            redir.addFlashAttribute("errorMessage", ex.getMessage());
            return null;
        }

        return responseEntity;
    }
    
    @GetMapping("/download")
    public ResponseEntity<Resource> download(HttpServletRequest request, RedirectAttributes redir, @RequestParam String id) throws ParseException {
        ResponseEntity<Resource> responseEntity;
        try {
            SuratKeluar suratKeluar;
            Optional<SuratKeluar> opSuratKeluar = suratKeluarDao.findById(id);
            if (opSuratKeluar.isPresent()) {
                suratKeluar = opSuratKeluar.get();
            } else {
                return null;
            }

            if (StringUtils.hasText(suratKeluar.getFile())) {
                String fileLocation = pdfService.uploadDir + File.separator + UploadFileService.SURAT_KELUAR + File.separator + suratKeluar.getFile();
                Resource file = FileReaderHelper.loadAsResource(fileLocation);
                responseEntity = ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + file.getFilename() + "\"").body(file);
            } else {
                return null;
            }
            return responseEntity;
        } catch (MalformedURLException ex) {
            redir.addFlashAttribute("errorMessage", ex.getMessage());
            return null;
        }
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redirectAttributes) {
        if (StringUtils.hasText(id)) {
            try {
                Optional<SuratKeluar> optionalSuratKeluar = suratKeluarDao.findById(id);
                if (optionalSuratKeluar.isPresent()) {
                    suratKeluarDao.delete(optionalSuratKeluar.get());
                    redirectAttributes.addFlashAttribute("successMessage", "Data berhasil dihapus");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("errorMessage", "Gagal menghapus data karena data tersebut berelasi dengan tabel lain");
            }
        }
        return "redirect:/" + SHOW_LIST;
    }

}
