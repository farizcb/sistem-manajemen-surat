package com.java.sistem.manajemen.surat.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "m_surat_keluar")
@Data
public class SuratKeluar extends BaseEntity {

    @Column(nullable = false, unique = true)
    private String kodeSurat;
    
    @Column(nullable = false, unique = true)
    private String noSurat;
    
    @ManyToOne
    @JoinColumn(name = "id_jenis_surat", nullable = false)
    private JenisSurat jenisSurat;
    
    @ManyToOne
    @JoinColumn(name = "id_surat_masuk")
    private SuratMasuk suratMasuk;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date tanggalSurat;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date tanggalCatat;
    
    @ManyToOne
    @JoinColumn(name = "id_instansi", nullable = false)
    private Instansi instansi;
    
    @Column(nullable = false)
    private String perihal;
    
    @Lob
    @Type(type = "text")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String deskripsi;
    
    private String file;
    
    @Transient
    private String strTanggalSurat;
    
}
