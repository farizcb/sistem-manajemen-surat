package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.JenisSurat;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface JenisSuratDao extends PagingAndSortingRepository<JenisSurat, String>, JpaSpecificationExecutor<JenisSurat> {

    JenisSurat findByJenisSurat(String jenisSurat);
    
}
