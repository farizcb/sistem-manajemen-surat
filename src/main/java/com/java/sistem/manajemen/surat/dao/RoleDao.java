package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String> {

}
