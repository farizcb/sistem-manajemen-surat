package com.java.sistem.manajemen.surat.rest;

import com.java.sistem.manajemen.surat.builder.DisposisiBuilder;
import com.java.sistem.manajemen.surat.dao.DisposisiDao;
import com.java.sistem.manajemen.surat.entity.Disposisi;
import com.java.sistem.manajemen.surat.helper.FileReaderHelper;
import com.java.sistem.manajemen.surat.service.DisposisiService;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/disposisi")
public class DisposisiRestController {
    
    @Autowired
    private DisposisiDao disposisiDao;
    
    @Autowired
    private DisposisiService disposisiService;
    
    private static final Logger logger = LoggerFactory.getLogger(DisposisiRestController.class);
    
    @PostMapping("/cetak")
    public Map<String, Object> cetakSuratMasuk(HttpServletRequest request, HttpServletResponse response) throws ParseException {
        List<Disposisi> listData;
        if (StringUtils.hasText(request.getParameter("keySearch")) && StringUtils.hasText(request.getParameter("valueSearch"))) {
            String key = request.getParameter("keySearch");
            String value = request.getParameter("valueSearch");
            DisposisiBuilder disposisiBuilder = new DisposisiBuilder();
            disposisiBuilder.with(key, "like", value);
            listData = disposisiDao.findAll(disposisiBuilder.build());
        } else {
            listData = (List<Disposisi>) disposisiDao.findAll();
        }

        try {
            String templateFile = request.getServletContext().getRealPath("/WEB-INF/excel/disposisi.xls");
            String fileReport = disposisiService.convertToExcel(templateFile, response, listData);
            return FileReaderHelper.responseFileExcel(new File(fileReport), Boolean.TRUE);
        } catch (IOException | InvalidFormatException e) {
            logger.error("Error cetak laporan agenda surat masuk : [{}]", e.getMessage());
            return null;
        }
    }
}
