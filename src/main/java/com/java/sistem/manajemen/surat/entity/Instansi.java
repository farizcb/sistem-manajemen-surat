package com.java.sistem.manajemen.surat.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "m_instansi")
@Data
public class Instansi extends BaseEntity {

    @Column(nullable = false)
    private String namaInstansi;
    
    @Lob
    @Type(type = "text")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String alamat;
    
    @Column(nullable = false, unique = true)
    private String noTelepon;
    
    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false, unique = true)
    private String kode;
    
}
