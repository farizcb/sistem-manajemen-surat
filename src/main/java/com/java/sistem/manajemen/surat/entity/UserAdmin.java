package com.java.sistem.manajemen.surat.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "m_user_admin")
@Data
public class UserAdmin extends BaseEntity {

    @OneToOne(orphanRemoval = true)
    @Cascade(CascadeType.ALL)
    @JoinColumn(name = "id_user", nullable = false)
    private User user;
    
    @Column(nullable = false, unique = true)
    private String noTelepon;
    
    @Column(nullable = false, unique = true)
    private String email;
    
    @Column(nullable = false, unique = true)
    private String npa;
    
    @Column(nullable = false)
    private String namaLengkap;

}
