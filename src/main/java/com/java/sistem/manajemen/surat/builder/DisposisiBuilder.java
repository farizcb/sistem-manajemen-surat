package com.java.sistem.manajemen.surat.builder;

import com.java.sistem.manajemen.surat.criteria.SearchCriteria;
import com.java.sistem.manajemen.surat.entity.Disposisi;
import com.java.sistem.manajemen.surat.spesification.DisposisiSpesification;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import static org.springframework.data.jpa.domain.Specification.where;

public class DisposisiBuilder {

    private final List<SearchCriteria> params;

    public DisposisiBuilder() {
        this.params = new ArrayList<>();
    }

    public DisposisiBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));

        return this;
    }

    public Specification<Disposisi> build() {
        if (params.isEmpty()) {
            return null;
        }

        List<Specification<Disposisi>> specs = new ArrayList<>();
        for (SearchCriteria param : params) {
            specs.add(new DisposisiSpesification(param));
        }

        Specification<Disposisi> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = where(result).and(specs.get(i));
        }
        return result;
    }

}
