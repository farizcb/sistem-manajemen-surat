package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.criteria.SearchCriteria;
import com.java.sistem.manajemen.surat.dao.RoleDao;
import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.dto.BasicSearchDto;
import com.java.sistem.manajemen.surat.dto.UserAdminDto;
import com.java.sistem.manajemen.surat.entity.User;
import com.java.sistem.manajemen.surat.entity.UserAdmin;
import com.java.sistem.manajemen.surat.entity.UserPassword;
import com.java.sistem.manajemen.surat.service.UserAdminService;
import com.java.sistem.manajemen.surat.spesification.UserAdminSpesification;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/user")
public class UserAdminController {

    private static final String SHOW_LIST = "user/list";
    private static final String SHOW_FORM = "user/form";

    @Autowired
    private UserAdminDao userAdminDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserAdminService userAdminService;

    @GetMapping("/list")
    public String showList(ModelMap modelMap, @PageableDefault Pageable pageable, BasicSearchDto basicSearchDto, Principal principal) {
        if (principal != null) {
            List<UserAdmin> listUserAdmin;
            if (StringUtils.hasText(basicSearchDto.getQuery())) {
                UserAdminSpesification userAdminSpesification = new UserAdminSpesification(new SearchCriteria(basicSearchDto.getQuery(), ":", basicSearchDto.getValue()));
                listUserAdmin = userAdminDao.findAll(userAdminSpesification, pageable).getContent();
            } else {
                listUserAdmin = userAdminDao.findAll(pageable).getContent();
            }
            List<UserAdmin> listData = userAdminService.getResultDataUserAdmin(listUserAdmin, userAdminDao.findByEmail(principal.getName()));
            modelMap.addAttribute("listData", userAdminService.convertListUserAdminToPage(listData, pageable));
            modelMap.addAttribute("paramSearch", basicSearchDto);
        }
        return SHOW_LIST;
    }

    @GetMapping("/form")
    public String showForm(ModelMap modelMap, @RequestParam(required = false) String id) {
        UserAdminDto userAdminDto = new UserAdminDto();
        if (id != null) {
            Optional<UserAdmin> opUserAdmin = userAdminDao.findById(id);
            if (opUserAdmin.isPresent()) {
                UserAdmin userAdmin = opUserAdmin.get();
                userAdminDto.setId(userAdmin.getId());
                userAdminDto.setNamaLengkap(userAdmin.getNamaLengkap());
                userAdminDto.setEmail(userAdmin.getEmail());
                userAdminDto.setNoTelepon(userAdmin.getNoTelepon());
                if (userAdmin.getUser().getRole().getId().equals("admin")) {
                    userAdminDto.setRole("admin");
                } else {
                    if (userAdmin.getUser().getTipe().equals("ketua")) {
                        userAdminDto.setRole("ketua");
                    } else {
                        userAdminDto.setRole("sekretaris");
                    }
                }
                userAdminDto.setNpa(userAdmin.getNpa());
                if (userAdmin.getUser().getActive()) {
                    userAdminDto.setStatus("aktif");
                } else {
                    userAdminDto.setStatus("non-aktif");
                }
            }
        }
        modelMap.addAttribute("userAdminDto", userAdminDto);
        modelMap.addAttribute("listRole", roleDao.findAll());
        return SHOW_FORM;
    }

    @PostMapping("/form")
    public String prosesForm(UserAdminDto userAdminDto, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        // validasi user
        Map<String, Object> mapValidasi = userAdminService.validasiSimpanUser(userAdminDto);
        boolean isValidate = (boolean) mapValidasi.get("isValidate");
        String message = (String) mapValidasi.get("message");
        if (isValidate) {
            modelMap.addAttribute("errorMessage", message);
            userAdminService.userAdminModel(modelMap, userAdminDto, roleDao.findAll());
            return SHOW_FORM;
        }

        // simpan user
        try {
            UserAdmin userAdmin = new UserAdmin();
            User u = new User();
            UserPassword userPassword = new UserPassword();

            if (StringUtils.isEmpty(userAdminDto.getId())) {
                if (StringUtils.hasText(userAdminDto.getPassword())) {
                    userPassword.setPassword(passwordEncoder.encode(userAdminDto.getPassword()));
                } else {
                    userAdminDto.setId(null);
                    modelMap.addAttribute("errorMessage", "Password tidak boleh kosong");
                    userAdminService.userAdminModel(modelMap, userAdminDto, roleDao.findAll());
                    return SHOW_FORM;
                }
            } else {
                userAdmin = userAdminDao.findById(userAdminDto.getId()).get();
                u = userAdmin.getUser();
                userPassword = userAdmin.getUser().getUserPassword();
                if (StringUtils.hasText(userAdminDto.getPassword())) {
                    userPassword.setPassword(passwordEncoder.encode(userAdminDto.getPassword()));
                } else {
                    userPassword.setPassword(u.getUserPassword().getPassword());
                }
            }

            if (StringUtils.hasText(userAdminDto.getPassword()) && !StringUtils.hasText(userAdminDto.getConfirmPassword())) {
                userAdminDto.setId(null);
                modelMap.addAttribute("errorMessage", "Konfirmasi Password tidak boleh kosong");
                userAdminService.userAdminModel(modelMap, userAdminDto, roleDao.findAll());
                return SHOW_FORM;
            }

            userPassword.setUser(u);
            u.setUsername(userAdminDto.getEmail());
            u.setUserPassword(userPassword);

            if ("aktif".equals(userAdminDto.getStatus())) {
                u.setActive(Boolean.TRUE);
            } else {
                u.setActive(Boolean.FALSE);
            }

            if (userAdminDto.getRole().equals("admin")) {
                u.setRole(roleDao.findById("admin").get());
                u.setTipe(null);
            } else {
                u.setRole(roleDao.findById("superadmin").get());
                if (userAdminDto.getRole().equals("ketua")) {
                    u.setTipe("ketua");
                } else {
                    u.setTipe("sekretaris");
                }
            }

            userAdmin.setNamaLengkap(userAdminDto.getNamaLengkap());
            userAdmin.setNoTelepon(userAdminDto.getNoTelepon());
            userAdmin.setEmail(userAdminDto.getEmail());
            userAdmin.setNpa(userAdminDto.getNpa());
            userAdmin.setUser(u);

            userAdminDao.save(userAdmin);
            String successMessage;
            if (StringUtils.hasText(userAdminDto.getId())) {
                successMessage = "Data berhasil diubah";
            } else {
                successMessage = "Data berhasil ditambah";
            }
            redirectAttributes.addFlashAttribute("successMessage", successMessage);
            return "redirect:/" + SHOW_LIST;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Data gagal disimpan karena : [" + e.getMessage() + "]");
            userAdminService.userAdminModel(modelMap, userAdminDto, roleDao.findAll());
            return SHOW_FORM;
        }
    }
    
    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redirectAttributes) {
        if (StringUtils.hasText(id)) {
            try {
                Optional<UserAdmin> optionalUserAdmin = userAdminDao.findById(id);
                if (optionalUserAdmin.isPresent()) {
                    userAdminDao.delete(optionalUserAdmin.get());
                    redirectAttributes.addFlashAttribute("successMessage", "Data berhasil dihapus");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("errorMessage", "Gagal menghapus data karena data tersebut berelasi dengan tabel lain");
            }
        }
        return "redirect:/" + SHOW_LIST;
    }
}
