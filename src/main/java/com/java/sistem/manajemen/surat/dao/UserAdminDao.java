package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.UserAdmin;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserAdminDao extends PagingAndSortingRepository<UserAdmin, String>, JpaSpecificationExecutor<UserAdmin> {

    UserAdmin findByNoTelepon(String noTelepon);

    UserAdmin findByEmail(String email);

    UserAdmin findByNpa(String npa);
    
    List<UserAdmin> findByUserRoleName(String nameRole);

}
