package com.java.sistem.manajemen.surat.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PdfService {
    
    @Value("${app.folder.upload}")
    public String uploadDir;
    
    public static final String SURAT_KELUAR = "surat_keluar/pdf";
    
    public String contentMaker(String content, Map<String, Object> params) throws IOException, TemplateException {
        Configuration cfg = new Configuration(new Version(2, 3, 26));
        // Some other recommended settings:
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        
        Template template = new Template("ftlNew", new StringReader(content), cfg);
        // Write output to the console
        Writer out = new StringWriter();
        template.process(params, out);
        
        String transformedTemplate = out.toString();
        return transformedTemplate;
    }
    
}
