package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.dao.UserAdminDao;
import com.java.sistem.manajemen.surat.dto.ProfileSayaDto;
import com.java.sistem.manajemen.surat.entity.UserAdmin;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/profile_saya")
public class ProfileSayaController {
    
    private static final String SHOW_PROFILE_SAYA = "profile_saya";
    
    @Autowired
    private UserAdminDao userAdminDao;
    
    @GetMapping("/form")
    public String showForm(@RequestParam String id, ModelMap modelMap) {
        Optional<UserAdmin> opUserAdmin = userAdminDao.findById(id);
        if (opUserAdmin.isPresent()) {
            ProfileSayaDto profileSayaDto = new ProfileSayaDto();
            profileSayaDto.setId(opUserAdmin.get().getId());
            profileSayaDto.setEmail(opUserAdmin.get().getEmail());
            profileSayaDto.setNamaLengkap(opUserAdmin.get().getNamaLengkap());
            profileSayaDto.setNoTelepon(opUserAdmin.get().getNoTelepon());
            profileSayaDto.setNpa(opUserAdmin.get().getNpa());
            modelMap.addAttribute("profileSayaDto", profileSayaDto);
        }
        return SHOW_PROFILE_SAYA;
    }
    
    @PostMapping("/form")
    public String prosesForm(ProfileSayaDto profileSayaDto, RedirectAttributes redirectAttributes, ModelMap modelMap) {
        Optional<UserAdmin> opUserAdmin = userAdminDao.findById(profileSayaDto.getId());
        if (opUserAdmin.isPresent()) {
            UserAdmin userAdmin = opUserAdmin.get();
            userAdmin.setEmail(profileSayaDto.getEmail());
            userAdmin.setNamaLengkap(profileSayaDto.getNamaLengkap());
            userAdmin.setNoTelepon(profileSayaDto.getNoTelepon());
            userAdmin.setNpa(profileSayaDto.getNpa());
            userAdmin.getUser().setUsername(profileSayaDto.getEmail());
            userAdminDao.save(userAdmin);
            redirectAttributes.addFlashAttribute("successMessage", "Data berhasil diubah");
        }
        return "redirect:/profile_saya/form?id=" + profileSayaDto.getId();
    }
    
}
