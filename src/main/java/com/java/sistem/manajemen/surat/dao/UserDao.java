package com.java.sistem.manajemen.surat.dao;

import com.java.sistem.manajemen.surat.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {

}
