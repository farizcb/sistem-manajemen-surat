package com.java.sistem.manajemen.surat.service;

import com.java.sistem.manajemen.surat.dao.JenisSuratDao;
import com.java.sistem.manajemen.surat.dao.SuratKeluarDao;
import com.java.sistem.manajemen.surat.dao.SuratMasukDao;
import com.java.sistem.manajemen.surat.entity.JenisSurat;

import java.util.*;

import com.java.sistem.manajemen.surat.helper.DateHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IndexService {

    @Autowired
    private JenisSuratDao jenisSuratDao;

    @Autowired
    private SuratMasukDao suratMasukDao;
    
    @Autowired
    private SuratKeluarDao suratKeluarDao;

    private final Logger LOGGER = LoggerFactory.getLogger(IndexService.class);

    public List<Map<String, Object>> getDataDounatChartSuratMasuk() {
        return addToMap("suratMasuk");
    }
    
    public List<Map<String, Object>> getDataDounatChartSuratKeluar() {
        return addToMap("suratKeluar");
    }

    private List<Map<String, Object>> addToMap(String tipe) {
        List<JenisSurat> listJenisSurat = (List<JenisSurat>) jenisSuratDao.findAll();
        List<Map<String, Object>> results = new ArrayList<>();
        for (JenisSurat jenisSurat : listJenisSurat) {
            Map<String, Object> map = new HashMap<>();
            List<Integer> listValue = new ArrayList<>();
            for (int i = 0; i < 12; i++) {
                Date startDate = DateHelper.getDynamicStartDateOfMonth(i + 1);
                Date endDate = DateHelper.getDynamicEndDateOfMonth(startDate);
                if (tipe.equals("suratMasuk")) {
                    listValue.add((int) suratMasukDao.countByJenisSuratIdAndTanggalSuratBetween(jenisSurat.getId(), startDate, endDate));
                } else {
                    listValue.add((int) suratKeluarDao.countByJenisSuratIdAndTanggalSuratBetween(jenisSurat.getId(), startDate, endDate));
                }
                map.put("listData", listValue);
            }
            map.put("name", jenisSurat.getJenisSurat());
            map.put("color", jenisSurat.getWarna());
            results.add(map);
        }
        return results;
    }

}
