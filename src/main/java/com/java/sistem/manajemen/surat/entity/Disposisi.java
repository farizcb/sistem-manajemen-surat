package com.java.sistem.manajemen.surat.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "m_disposisi")
@Data
public class Disposisi extends BaseEntity {

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "id_surat_masuk", nullable = false)
    private SuratMasuk suratMasuk;
    
    @Column(nullable = false)
    private String tujuanDisposisi;
    
    @Column(nullable = false)
    private String sifat;
    
    @Lob
    @Type(type = "text")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String isi;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date batasWaktu;
    
    @Lob
    @Type(type = "text")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String informasi;
    
    @Transient
    private String strBatasWaktu;
    
}
