package com.java.sistem.manajemen.surat.controller;

import com.java.sistem.manajemen.surat.criteria.SearchCriteria;
import com.java.sistem.manajemen.surat.dao.InstansiDao;
import com.java.sistem.manajemen.surat.dto.BasicSearchDto;
import com.java.sistem.manajemen.surat.entity.Instansi;
import com.java.sistem.manajemen.surat.helper.AppHelper;
import com.java.sistem.manajemen.surat.service.InstansiService;
import com.java.sistem.manajemen.surat.spesification.InstansiSpesification;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/instansi")
public class InstansiController {

    private static final String SHOW_LIST = "instansi/list";
    private static final String SHOW_FORM = "instansi/form";

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private InstansiService instansiService;

    private static final Logger logger = LoggerFactory.getLogger(InstansiController.class);

    @GetMapping("/list")
    public String showList(ModelMap modelMap, @PageableDefault Pageable pageable, BasicSearchDto basicSearchDto) {
        if (StringUtils.hasText(basicSearchDto.getQuery())) {
            InstansiSpesification instansiSpesification = new InstansiSpesification(new SearchCriteria(basicSearchDto.getQuery(), ":", basicSearchDto.getValue()));
            modelMap.addAttribute("listData", instansiDao.findAll(instansiSpesification, pageable));
        } else {
            modelMap.addAttribute("listData", instansiDao.findAll(pageable));
        }
        modelMap.addAttribute("paramSearch", basicSearchDto);
        return SHOW_LIST;
    }

    @GetMapping("/form")
    public String showForm(ModelMap modelMap, @RequestParam(required = false) String id) {
        Instansi i = new Instansi();
        if (StringUtils.hasText(id)) {
            Optional<Instansi> opInstansi = instansiDao.findById(id);
            if (opInstansi.isPresent()) {
                i = opInstansi.get();
            }
        }
        modelMap.addAttribute("instansi", i);
        return SHOW_FORM;
    }

    @PostMapping("/form")
    public String prosesForm(Instansi instansi, ModelMap modelMap, RedirectAttributes redirectAttributes) {
        // validasi instansi
        Map<String, Object> mapValidasi = instansiService.validasiSimpanInstansi(instansi);
        boolean isValidate = (boolean) mapValidasi.get("isValidate");
        String message = (String) mapValidasi.get("message");
        if (isValidate) {
            modelMap.addAttribute("errorMessage", message);
            modelMap.addAttribute("instansi", instansi);
            return SHOW_FORM;
        }

        // simpan instansi
        try {
            logger.info("do save instansi : [{}]", instansi);
            instansiDao.save(instansi);
            redirectAttributes.addFlashAttribute("successMessage", AppHelper.successMessage(instansi.getId()));
            return "redirect:/" + SHOW_LIST;
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Data gagal disimpan karena : [" + e.getMessage() + "]");
            modelMap.addAttribute("instansi", instansi);
            return SHOW_FORM;
        }
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redirectAttributes) {
        if (StringUtils.hasText(id)) {
            try {
                Optional<Instansi> optionalInstansi = instansiDao.findById(id);
                if (optionalInstansi.isPresent()) {
                    instansiDao.delete(optionalInstansi.get());
                    redirectAttributes.addFlashAttribute("successMessage", "Data berhasil dihapus");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("errorMessage", "Gagal menghapus data karena data tersebut berelasi dengan tabel lain");
            }
        }
        return "redirect:/" + SHOW_LIST;
    }

}
